using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Parsing;
using System.Linq;

namespace MfGames.ToolBuilder.Extensions
{
    public static class ParseResultExtensions
    {
        public static List<string> GetValueListForOption(
            this ParseResult result,
            Option<string> option)
        {
            string? optionValues = result.GetValueForOption(option);

            if (optionValues == null)
            {
                return new List<string>();
            }

            var values = optionValues
                .Split(',')
                .Select(x => x.Trim())
                .SelectMany(x => x.Split(' '))
                .Select(x => x.Trim())
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .ToList();

            return values;
        }
    }
}
