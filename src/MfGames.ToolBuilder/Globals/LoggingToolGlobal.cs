using System;
using System.CommandLine;

using MfGames.ToolBuilder.Extensions;

using Serilog;
using Serilog.Core;
using Serilog.Events;
using Serilog.Exceptions;

namespace MfGames.ToolBuilder.Services
{
    /// <summary>
    /// A service for handling logging options.
    /// </summary>
    public class LoggingToolGlobal
    {
        public LoggingToolGlobal()
        {
            this.LogLevelOption = new Option<string>(
                "--log-level",
                () => nameof(LogEventLevel.Information),
                string.Format(
                    "Controls the verbosity of the output: {0} (defaults to Warning). Not case-sensitive and prefixes allowed.",
                    string.Join(", ", Enum.GetNames<LogEventLevel>())));
        }

        /// <summary>
        /// Gets the common option for setting the log level.
        /// </summary>
        public Option<string> LogLevelOption { get; }

        /// <summary>
        /// Adds the common options to the command.
        /// </summary>
        /// <param name="root"></param>
        public void Attach(Command root)
        {
            root.AddGlobalOption(this.LogLevelOption);
        }

        /// <summary>
        /// Sets up logging based on the global settings.
        /// </summary>
        /// <param name="arguments">The arguments to the command.</param>
        public void Configure(string[] arguments)
        {
            // Figure out the logging level.
            string level = GlobalOptionHelper.GetArgumentValue(
                this.LogLevelOption,
                arguments,
                "Warning");
            LogEventLevel logLevel = level.GetEnumFuzzy<LogEventLevel>(
                "log level");

            // Figure out how we are going to configure the logger.
            var configuration = new LoggerConfiguration();

            // Create the logger and set it.
            const string template =
                "{Timestamp:yyyy-MM-dd HH:mm:ss} [{Level:u3}] "
                + "{Message}"
                + "{NewLine}{Exception}";

            Logger logger = configuration
                .Enrich.WithDemystifiedStackTraces()
                .Enrich.WithExceptionDetails()
                .MinimumLevel.Is(logLevel)
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .Enrich.FromLogContext()
                .WriteTo.Console(outputTemplate: template)
                .CreateLogger();

            Log.Logger = logger;
        }
    }
}
