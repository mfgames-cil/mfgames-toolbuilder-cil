using System;
using System.Collections.Generic;
using System.Linq;

using FluentResults;

namespace MfGames.ToolBuilder
{
    public class ToolException : Exception
    {
        public ToolException(IEnumerable<string> messages)
            : this(messages.ToArray())
        {
        }

        public ToolException(params string[] messages)
            : base("There was an exception while processing the tool.")
        {
            this.Messages = messages;
        }

        public ToolException()
            : base("There was an exception while processing the tool.")
        {
            this.Messages = Array.Empty<string>();
        }

        public ToolException(string? message)
            : this()
        {
            this.Messages = message != null
                ? new[] { message }
                : Array.Empty<string>();
        }

        public ToolException(string? message, Exception? innerException)
            : base(
                "There was an exception while processing the tool.",
                innerException)
        {
            this.Messages = message != null
                ? new[] { message }
                : Array.Empty<string>();
        }

        public int ExitCode { get; } = 1;

        public string[] Messages { get; }

        public bool SuppressStackTrace { get; set; } = true;

        public static void ThrowIf(IEnumerable<ResultBase> results)
        {
            var errors = results
                .Where(x => x.IsFailed)
                .ToList();

            if (errors.Count == 0)
            {
                return;
            }

            IEnumerable<string> messages = errors
                .SelectMany(x => x.Errors)
                .Select(x => x.Message);

            throw new ToolException(messages);
        }
    }
}
