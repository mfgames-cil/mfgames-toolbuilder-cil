using Autofac;

using MfGames.ToolBuilder.Services;

namespace MfGames.ToolBuilder
{
    /// <summary>
    /// The Autofac module to pull in the components inside this assembly.
    /// </summary>
    public class ToolBuilderModule : Module
    {
        /// <inheritdoc />
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterAssemblyTypes(this.GetType().Assembly)
                .Except<ConfigToolGlobal>()
                .Except<LoggingToolGlobal>()
                .AsSelf()
                .AsImplementedInterfaces();
        }
    }
}
