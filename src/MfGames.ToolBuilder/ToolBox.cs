using System;
using System.CommandLine.Parsing;
using System.Threading.Tasks;

using Serilog;

namespace MfGames.ToolBuilder;

/// <summary>
/// A collection of tools set up for running the command-line shell.
/// </summary>
public class ToolBox
{
    private readonly string[] arguments;

    private readonly Parser parser;

    public ToolBox(string[] arguments, Parser parser)
    {
        this.arguments = arguments;
        this.parser = parser;
    }

    /// <summary>
    /// Finishes building the tool, parses the arguments, and runs the
    /// command.
    /// </summary>
    /// <returns>An error code, 0 for successful, otherwise false.</returns>
    public async Task<int> RunAsync()
    {
        try
        {
            ParseResult parseResults = this.parser.Parse(this.arguments);
            int exitCode = await parseResults.InvokeAsync();

            return exitCode;
        }
        catch (Exception exception)
        {
            Log.Fatal(
                exception,
                "There was a problem running the command: {Arguments}",
                this.arguments);

            return Environment.ExitCode == 0 ? 1 : Environment.ExitCode;
        }

        // Get the exit code and return it.
        // TODO return Environment.ExitCode;
    }
}
