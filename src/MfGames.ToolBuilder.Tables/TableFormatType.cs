namespace MfGames.ToolBuilder.Tables
{
    /// <summary>
    /// A list of the known formats. This duplicates the values in
    /// `ConsoleTableBuilderFormat` in addition to adding
    /// our additional formats.
    /// </summary>
    public enum TableFormatType
    {
        /// <summary>
        /// Duplicates ConsoleTableBuilderFormat.Default.
        /// </summary>
        Default,

        /// <summary>
        /// Duplicates ConsoleTableBuilderFormat.MarkDown.
        /// </summary>
        Markdown,

        /// <summary>
        /// Duplicates ConsoleTableBuilderFormat.Alternative.
        /// </summary>
        Alternative,

        /// <summary>
        /// Duplicates ConsoleTableBuilderFormat.Minimal.
        /// </summary>
        Minimal,

        /// <summary>
        /// Indicates that the output should be written as a JSON structure.
        /// </summary>
        Json,

        /// <summary>
        /// Indicates that the output should be written as a PowerShell-style list.
        /// </summary>
        List,

        /// <summary>
        /// Indicates tht the output should be written as comma-separated values.
        /// </summary>
        Csv,
    }
}
