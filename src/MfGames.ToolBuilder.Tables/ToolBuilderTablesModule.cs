using Autofac;

namespace MfGames.ToolBuilder.Tables;

/// <summary>
/// The Autofac module to pull in the components inside this assembly.
/// </summary>
public class ToolBuilderTablesModule : Module
{
    /// <inheritdoc />
    protected override void Load(ContainerBuilder builder)
    {
        builder
            .RegisterType<TableToolService>()
            .AsSelf()
            .AsImplementedInterfaces();
    }
}
