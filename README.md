# MfGames.ToolBuilder

_An opinionated library for easily creating command-line tools in C#._

MfGames.ToolBuilder is a library to encapsulate the setup and running of tools, CLI components arranged with verbs (like `git`). It includes some opinionated decisions on default setup.

## Building

To make sure the environment is correctly set up for build:

```sh
./scripts/setup.sh
```

To build the project:

```sh
./scripts/build.sh
```

To test the project:

```sh
./scripts/test.sh
```

## Commands

This library is built on top of [System.CommandLine](https://github.com/dotnet/command-line-api) and .NET 6 generic hosting. The commands are combined together using dependency injection (see below) which allows for them to be included as parameters for the constructor or dynamically discovered.

All commands are assumed to be asynchronous and include a cancellation token in their calls.

`System.CommandLine` was chosen because it allows for composition of features instead on needing inheritance to give a tool the ability to render a table or handle additional features.

## Autofac

While most libraries should only use the included service provider infrastructure with the base library, this tool sets up [Autofac](https://autofac.org/) to handle services. This allows for both service provider usage (via `ConfigureServices`) and Autofac (via `ConfigureContainer`).

Autofac was chosen because of a number of quality of life features and personal preferences of the developer. A tool can easily ignore that functionality in favor of using only `IServiceProvider`.

## Serilog

Likewise, tools are set up to use [Serilog](https://serilog.net/) for logging instead of the built-in logging code. However, both are served by Serilog so `ILoggerFactory<T>` can be used.

Like Autofac, Serilog was given because of the variety of sinks, certain quality of life, but also the ability to log more detailed objects. It also produces some coloring to log details based on data types.

To use the Serilog functionality with `ILoggerFactory`, objects needed to be included as parameters instead of inlined with string interpolation.
