#!/usr/bin/env sh

# Normalize our environment.
cd $(dirname $0)/..

# Make sure we have the needed executables installed.
for e in dotnet lefthook prettier nixfmt
do
  if ! which $e >& /dev/null
  then
      echo "Cannot find '$e' in the path"
      exit 1
  fi
done

# Make sure we have lefthook is installed.
lefthook install

# Make sure our tools are installed.
dotnet tool restore

# Everything is good.
exit 0
