#!/usr/bin/env sh

cd $(dirname $0)/..
./scripts/setup.sh || exit 1

dotnet build
