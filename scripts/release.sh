#!/usr/bin/env sh

cd $(dirname $0)/..
./scripts/setup.sh || exit 1

# Verify the input.
if [ "x$NUGET_TOKEN" = "x" ]
then
    echo "the environment variable NUGET_TOKEN is not defined"
    exit 1
fi

if [ "x$NUGET_PUSH_URL" = "x" ]
then
    echo "the environment variable NUGET_PUSH_URL is not defined"
    exit 1
fi

# Clean up everything from the previous runs.
dotnet clean

# Version the file based on the Git repository.
(cd src && dotnet dotnet-gitversion /updateprojectfiles)
SEMVER="v$(dotnet gitversion /output json | jq -r .SemVer)"

if [ "x$SEMVER" = "x" ]
then
  echo "cannot figure out the semantic version"
  exit 1
fi

# Build to pick up the new version.
dotnet build

# Create and publish the NuGet packages.
dotnet pack --include-symbols --include-source
dotnet nuget push src/*/bin/Debug/*.nupkg --api-key $NUGET_TOKEN --source $NUGET_PUSH_URL

# Tag and push, but only if we don't have a tag.
if ! git tag | grep $SEMVER >& /dev/null
then
  echo "tagging and pushing"
  git tag $SEMVER
  git push origin $SEMVER
else
  echo "not tagging, already exists"
fi
