using System.CommandLine;
using System.CommandLine.Invocation;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using ILogger = Serilog.ILogger;

namespace SampleTool;

public class LogCommand : Command, ICommandHandler
{
    private readonly ILogger<LogCommand> extensionLogger;

    private readonly ILogger serilogLogger;

    /// <inheritdoc />
    public LogCommand(
        ILoggerFactory loggerFactory,
        ILogger serilogLogger)
        : base("log", "Shows various logging messages.")
    {
        this.serilogLogger = serilogLogger;
        this.extensionLogger = loggerFactory.CreateLogger<LogCommand>();
        this.Handler = this;
    }

    /// <inheritdoc />
    public Task<int> InvokeAsync(InvocationContext context)
    {
        // Show the serilog logging.
        this.serilogLogger.Debug("Serilog Debug");
        this.serilogLogger.Error("Serilog Error");
        this.serilogLogger.Fatal("Serilog Fatal");
        this.serilogLogger.Information("Serilog Information");
        this.serilogLogger.Verbose("Serilog Verbose");
        this.serilogLogger.Warning("Serilog Warning");

        // Show the extension logging.
        this.extensionLogger.LogCritical(
            "System.Extension.Logging LogCritical");
        this.extensionLogger.LogDebug(
            "System.Extension.Logging LogDebug");
        this.extensionLogger.LogError(
            "System.Extension.Logging LogError");
        this.extensionLogger.LogInformation(
            "System.Extension.Logging LogInformation");
        this.extensionLogger.LogTrace(
            "System.Extension.Logging LogTrace");
        this.extensionLogger.LogWarning(
            "System.Extension.Logging LogWarning");

        // Show Serilog working through logging extensions.
        var hash = new { Number = 1, String = "String" };

        this.extensionLogger.LogInformation(
            "Contextual information via {Name} and {Quotes:l}",
            "extension logger",
            "without quotes");
        this.extensionLogger.LogInformation(
            "Contextual information via {@Nested}",
            hash);

        // We're good.
        return Task.FromResult(0);
    }
}
