using System.CommandLine;

using Autofac;

using SampleTool;

namespace MfGames.ToolBuilder
{
    /// <summary>
    /// The Autofac module to pull in the components inside this assembly.
    /// </summary>
    public class SampleToolModule : Module
    {
        /// <inheritdoc />
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterAssemblyTypes(this.GetType().Assembly)
                .AsSelf()
                .AsImplementedInterfaces();

            builder
                .Register(
                    c =>
                    {
                        var root = new RootCommand
                        {
                            Name = "sample-tool",
                            Description =
                                "A sample tool that demonstrates functionality",
                        };

                        root.AddCommand(c.Resolve<CrashCommand>());
                        root.AddCommand(c.Resolve<TableCommand>());
                        root.AddCommand(c.Resolve<LogCommand>());

                        return root;
                    })
                .AsSelf();
        }
    }
}
