using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.Data;
using System.Threading.Tasks;

using MfGames.ToolBuilder.Tables;

namespace SampleTool
{
    public class TableCommand : Command, ICommandHandler
    {
        private readonly DataTable table;

        private readonly TableToolService tableService;

        /// <inheritdoc />
        public TableCommand(TableToolService.Factory tableService)
            : base("table", "Display a table.")
        {
            // Create the table structure.
            this.table = new DataTable();
            this.table.Columns.Add("DefaultString", typeof(string));
            this.table.Columns.Add("DefaultInt32", typeof(int));
            this.table.Columns.Add("HiddenString", typeof(string));

            // Create the table service for formatting and displaying results.
            this.tableService = tableService(
                this,
                this.table,
                new List<string>
                {
                    "DefaultString",
                    "DefaultInt32",
                });

            // This class handles the command.
            this.Handler = this;
        }

        /// <inheritdoc />
        public Task<int> InvokeAsync(InvocationContext context)
        {
            this.table.Rows.Add("Row 1", 1, "Hidden 1");
            this.table.Rows.Add("Row 2", 10, "Hidden 2");
            this.table.Rows.Add("Row 3", 100, "Hidden 3");

            this.tableService.Write(context);

            return Task.FromResult(0);
        }
    }
}
