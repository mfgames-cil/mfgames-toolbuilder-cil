using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.Threading.Tasks;

using MfGames.ToolBuilder;

namespace SampleTool
{
    public class CrashCommand : Command, ICommandHandler
    {
        private readonly Option<bool> messyOption;

        /// <inheritdoc />
        public CrashCommand()
            : base("crash", "Crash the application with an exception.")
        {
            this.Handler = this;
            this.messyOption = new Option<bool>("--messy");

            this.AddOption(this.messyOption);
        }

        /// <inheritdoc />
        public Task<int> InvokeAsync(InvocationContext context)
        {
            bool messy = context.ParseResult.GetValueForOption(this.messyOption);

            if (messy)
            {
                throw new Exception(
                    "This command crashed messily as requested.");
            }

            throw new ToolException("This command crashed as requested.");
        }
    }
}
