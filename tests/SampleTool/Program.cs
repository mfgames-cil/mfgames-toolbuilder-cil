using System.Threading.Tasks;

using Autofac;

using MfGames.ToolBuilder;
using MfGames.ToolBuilder.Tables;

namespace SampleTool
{
    public static class Program
    {
        public static async Task<int> Main(string[] args)
        {
            return await ToolBoxBuilder
                .Create("SampleTool", args)
                .ConfigureContainer(ConfigureContainer)
                .Build()
                .RunAsync();
        }

        private static void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule<SampleToolModule>();
            builder.RegisterModule<ToolBuilderTablesModule>();
        }
    }
}
