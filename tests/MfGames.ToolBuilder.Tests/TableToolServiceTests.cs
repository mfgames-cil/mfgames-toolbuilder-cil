using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.CommandLine.Parsing;
using System.Data;

using MfGames.TestSetup;
using MfGames.ToolBuilder.Tables;

using Xunit;
using Xunit.Abstractions;

namespace MfGames.ToolBuilder.Tests
{
    public class TableToolServiceTests : TestBase<ToolBuilderTestContext>
    {
        private readonly DataTable table;

        public TableToolServiceTests(ITestOutputHelper output)
            : base(output)
        {
            // Create the table structure.
            this.table = new DataTable();
            this.table.Columns.Add("DefaultString", typeof(string));
            this.table.Columns.Add("DefaultInt32", typeof(int));
            this.table.Columns.Add("HiddenString", typeof(string));
            this.table.Rows.Add("Row 1", 1, "Hidden 1");
            this.table.Rows.Add("Row 2", 10, "Hidden 2");
            this.table.Rows.Add("Row 3", 100, "Hidden 3");
        }

        [Fact]
        public void ChooseAll()
        {
            // Set up the test.
            using ToolBuilderTestContext context = this.CreateContext();

            // Create the command with the table.
            (var service, InvocationContext invoke) = this.InvokeCommand(
                context,
                "--table-columns",
                "*");

            // Verify the results.
            Assert.Equal(
                new[]
                {
                    "DefaultString",
                    "DefaultInt32",
                    "HiddenString",
                },
                service.GetVisibleColumnNames(invoke));
            Assert.True(service.IsVisible(invoke, "DefaultString"));
            Assert.True(service.IsVisible(invoke, "DefaultInt32"));
            Assert.True(service.IsVisible(invoke, "HiddenString"));
        }

        [Fact]
        public void ChooseExactMatch()
        {
            // Set up the test.
            using ToolBuilderTestContext context = this.CreateContext();

            // Create the command with the table.
            (var service, InvocationContext invoke) = this.InvokeCommand(
                context,
                "--table-columns",
                "DefaultInt32");

            // Verify the results.
            Assert.Equal(
                new[]
                {
                    "DefaultInt32",
                },
                service.GetVisibleColumnNames(invoke));
            Assert.False(service.IsVisible(invoke, "DefaultString"));
            Assert.True(service.IsVisible(invoke, "DefaultInt32"));
            Assert.False(service.IsVisible(invoke, "HiddenString"));
        }

        [Fact]
        public void ChooseGlob()
        {
            // Set up the test.
            using ToolBuilderTestContext context = this.CreateContext();

            // Create the command with the table.
            (var service, InvocationContext invoke) = this.InvokeCommand(
                context,
                "--table-columns",
                "*string");

            // Verify the results.
            Assert.Equal(
                new[]
                {
                    "DefaultString",
                    "HiddenString",
                },
                service.GetVisibleColumnNames(invoke));
            Assert.True(service.IsVisible(invoke, "DefaultString"));
            Assert.False(service.IsVisible(invoke, "DefaultInt32"));
            Assert.True(service.IsVisible(invoke, "HiddenString"));
        }

        [Fact]
        public void DefaultColumns()
        {
            // Set up the test.
            using ToolBuilderTestContext context = this.CreateContext();

            // Create the command with the table.
            (var service, InvocationContext invoke) =
                this.InvokeCommand(context);

            // Verify the results.
            Assert.Equal(
                new[]
                {
                    "DefaultString",
                    "DefaultInt32",
                },
                service.GetVisibleColumnNames(invoke));
            Assert.True(service.IsVisible(invoke, "DefaultString"));
            Assert.True(service.IsVisible(invoke, "DefaultInt32"));
            Assert.False(service.IsVisible(invoke, "HiddenString"));
        }

        private (TableToolService service, InvocationContext invoke)
            InvokeCommand(
                ToolBuilderTestContext context,
                params string[] arguments)
        {
            TableToolService.Factory serviceFactory =
                context.Resolve<TableToolService.Factory>();
            var command = new RootCommand();
            TableToolService service = serviceFactory(
                command,
                this.table,
                new List<string>
                {
                    "DefaultString",
                    "DefaultInt32",
                });
            ParseResult results = command.Parse(arguments);
            var invoke = new InvocationContext(results);
            return (service, invoke);
        }
    }
}
