using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using CliWrap;
using CliWrap.Exceptions;

using MfGames.IO.Extensions;

using Xunit;

namespace MfGames.ToolBuilder.Tests
{
    /// <summary>
    /// Tests the SampleTool in the tests directory to make sure the
    /// basic functionality is correct.
    /// </summary>
    public class SampleToolTests
    {
        [Fact]
        public void CrashCommandFails()
        {
            // Run the executable using CliWrap.
            FileInfo projectFile = this.GetProjectFile();
            StringBuilder output = new();
            CancellationToken cancellationToken =
                new CancellationTokenSource(TimeSpan.FromSeconds(20))
                    .Token;

            Task<CommandExecutionException> exception =
                Assert.ThrowsAsync<CommandExecutionException>(
                    async () => await Cli.Wrap("dotnet")
                        .WithArguments(
                            new[]
                            {
                                "run",
                                "--project", projectFile.FullName,
                                "--",
                                "crash",
                            })
                        .WithWorkingDirectory(projectFile.DirectoryName!)
                        .WithStandardOutputPipe(
                            PipeTarget.ToStringBuilder(output))
                        .ExecuteAsync(cancellationToken)
                        .ConfigureAwait(false));

            // Verify the return code.
            Assert.NotNull(exception);
        }

        [Fact]
        public async Task TableCommandWorks()
        {
            // Run the executable using CliWrap.
            FileInfo projectFile = this.GetProjectFile();
            StringBuilder output = new();
            var delay = TimeSpan.FromMinutes(1);
            CancellationTokenSource cancellationTokenSource = new(delay);
            CancellationToken cancellationToken = cancellationTokenSource.Token;

            CommandResult result = await Cli.Wrap("dotnet")
                .WithArguments(
                    new[]
                    {
                        "run", "--project", projectFile.FullName, "--", "table",
                    })
                .WithWorkingDirectory(projectFile.DirectoryName!)
                .WithStandardOutputPipe(PipeTarget.ToStringBuilder(output))
                .ExecuteAsync(cancellationToken)
                .ConfigureAwait(false);

            // Verify the return code.
            Assert.Equal(0, result.ExitCode);

            // Check the output.
            Assert.Equal(
                new[]
                {
                    "DefaultString DefaultInt32",
                    "-------------+------------",
                    "Row 1                    1",
                    "Row 2                   10",
                    "Row 3                  100",
                    "",
                },
                output.ToString().Split("\n"));
        }

        /// <summary>
        /// Gets the file object representing the sample tool's project.
        /// </summary>
        private FileInfo GetProjectFile()
        {
            FileInfo file = this
                .GetType()
                .GetDirectory()
                .FindGitRoot()
                !.GetFile("tests", "SampleTool", "SampleTool.csproj");

            return file;
        }
    }
}
