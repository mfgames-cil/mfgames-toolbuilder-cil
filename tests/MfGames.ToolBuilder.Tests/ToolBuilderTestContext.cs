using Autofac;

using MfGames.TestSetup;
using MfGames.ToolBuilder.Tables;

namespace MfGames.ToolBuilder.Tests
{
    public class ToolBuilderTestContext : TestContext
    {
        /// <inheritdoc />
        protected override void ConfigureContainer(ContainerBuilder builder)
        {
            base.ConfigureContainer(builder);
            builder.RegisterModule<ToolBuilderModule>();
            builder.RegisterModule<ToolBuilderTablesModule>();
        }
    }
}
